from django.contrib import admin
from .models import Usuario, Rol, VehiculoUsuario, Vehiculo, ModeloVehiculo, MarcaVehiculo

# Register your models here.
@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    list_display = [

    ]

@admin.register(Rol)
class RolAdmin(admin.ModelAdmin):
    list_display = [
        
    ]

@admin.register(VehiculoUsuario)
class VehiculoUsuarioAdmin(admin.ModelAdmin):
    list_display = [
        
    ]

@admin.register(Vehiculo)
class VehiculoAdmin(admin.ModelAdmin):
    list_display = [
        "numSerie",
        "placas",
        "kilometraje",
        "fechaAdquisicion",
        "usaDiesel",
        "conductor",
    ]

@admin.register(ModeloVehiculo)
class ModeloVehiculoAdmin(admin.ModelAdmin):
    list_display = [
        "num",
        "nombre",
        "consumoCombustible",
        "cantCarga",
        "capPasajeros",
        "vehiculo",
    ]

@admin.register(MarcaVehiculo)
class MarcaVehiculoAdmin(admin.ModelAdmin):
    list_display = [
        "num",
        "nombre",
        "vehiculo",
    ]