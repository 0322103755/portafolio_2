from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from datetime import time, timedelta
from django.dispatch import receiver

# Create your models here.

#MODEL ROL
class Rol(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)

#MODEL USER
class Usuario(models.Model):
    num = models.AutoField(primary_key=True)
    nombrePila = models.CharField(max_length=255)
    apPaterno = models.CharField(max_length=255)
    apMaterno = models.CharField(max_length=255)
    numTel = models.CharField(max_length=20)
    correo = models.EmailField()
    contrasena = models.CharField(max_length=255)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)

def __str__(self):
  return f'{self.nombrePila} {self.apPaterno} {self.apMaterno} - {self.rol}'

 
#MODEL VEHICLE
class Vehiculo(models.Model):
    numSerie = models.CharField(max_length=30, primary_key=True)
    placas = models.CharField(max_length=255)
    kilometraje = models.IntegerField()
    fechaAdquisicion = models.DateField()
    usaDiesel = models.BooleanField()
    conductor = models.ForeignKey(Usuario, on_delete=models.CASCADE)

def __str__(self):
  return f'{self.numSerie}'


#MODELO MARCAVEHICULO
class MarcaVehiculo(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    vehiculo = models.ForeignKey(Vehiculo, on_delete=models.CASCADE)

def __str__(self):
  return f'{self.nombre}'


#MODELO MODELOVEHICULO
class ModeloVehiculo(models.Model):
    num = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    consumoCombustible = models.FloatField()
    cantCarga = models.IntegerField()
    capPasajeros = models.IntegerField()
    vehiculo = models.ForeignKey(Vehiculo, on_delete=models.CASCADE)

def __str__(self):
  return f'{self.nombre}'
 

#MODELO VEHICULOUSUARIO
class VehiculoUsuario(models.Model):
    vehiculo = models.ForeignKey(Vehiculo, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("vehiculo", "usuario"),)
