# Generated by Django 3.2.19 on 2023-11-06 04:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_alter_marcavehiculo_num_alter_modelovehiculo_num'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marcavehiculo',
            name='num',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='modelovehiculo',
            name='num',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='rol',
            name='num',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='usuario',
            name='num',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='vehiculo',
            name='numSerie',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
