from django.shortcuts import render
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.views import generic
from django.views.generic import ListView
from django.urls import reverse_lazy
from .models import Usuario, Rol, Vehiculo, VehiculoUsuario, MarcaVehiculo, ModeloVehiculo
from .forms import UpdateUsuarioForm, NewUsuarioForm, UpdateRolForm, NewRolForm, UpdateVehiculoUsuarioForm, NewVehiculoUsuarioForm, UpdateVehiculoForm, NewVehiculoForm, UpdateModeloVehiculoForm, NewModeloVehiculoForm, UpdateMarcaVehiculoForm, NewMarcaVehiculoForm

# Create your views here.
# def index(request):
#   return render(request, "home/index.html", {})

#PLANTILLA RETORNO
class Base(generic.View):
    template_name="base/base.html"
    context= {}

    def get(self, request):
        return render(request, self.template_name, self.context)
    
#PLANTILLA PARA VEHICULO
#HOME/VEHICULO
class NewVehiculo(generic.CreateView):
    template_name = "home/new_vehiculo.html"
    model = Vehiculo
    form_class = NewVehiculoForm
    success_url = reverse_lazy("home:vehiculoindex")#Aqui es a donde quieras retornar cuando se delete


class ListarVehiculo(generic.View):
    template_name = "home/vehiculos.html"
    context = {}

    def get(self, request):
        self.context = {
            "vehiculo": Vehiculo.objects.all()
        }
        return render(request, self.template_name, self.context)
    
#PENDIENTE
class DetailVehiculo(generic.View):
    template_name = "home/detail_vehiculo.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "vehiculo": get_object_or_404(Vehiculo, numSerie=pk)
        }
        return render(request, self.template_name, self.context)


class DeleteVehiculo(generic.DeleteView):
    template_name = "home/delete_vehiculo.html"
    model = Vehiculo
    success_url = reverse_lazy("home:vehiculoindex")
    success_message = "Vehiculo was deleted successfully."

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)



class UpdateVehiculo(generic.UpdateView):
    template_name = "home/update_vehiculo.html"
    model = Vehiculo
    form_class = UpdateVehiculoForm
    success_url = reverse_lazy("home:vehiculoindex")

    def get_object(self, queryset=None):
        return get_object_or_404(Vehiculo, numSerie=self.kwargs['pk'])
    

#PLANTILLA PARA MARCAVEHICULO
class NewMarcaVehiculo(generic.CreateView):
    template_name = "home/new_marcavehiculo.html"
    model = MarcaVehiculo
    form_class = NewMarcaVehiculoForm
    success_url = reverse_lazy("home:marcavehiculoindex")


class ListarMarcaVehiculo(generic.View):
    template_name = "home/marcavehiculos.html"
    context = {}

    def get(self, request):
        self.context = {
            "marcavehiculo": MarcaVehiculo.objects.all()
        }
        return render(request, self.template_name, self.context)


class DetailMarcaVehiculo(generic.View):
    template_name = "home/detail_marcavehiculo.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "marcavehiculo": get_object_or_404(MarcaVehiculo, num=pk)
        }
        return render(request, self.template_name, self.context)


class DeleteMarcaVehiculo(generic.DeleteView):
    template_name = "home/delete_marcavehiculo.html"
    model = MarcaVehiculo
    success_url = reverse_lazy("home:marcavehiculoindex")
    

class UpdateMarcaVehiculo(generic.UpdateView):
    template_name = "home/update_marcavehiculo.html"
    model = MarcaVehiculo
    form_class = UpdateMarcaVehiculoForm
    success_url = reverse_lazy("home:marcavehiculoindex")
    

#PLANTILLA PARA MODELOVEHICULO
class NewModeloVehiculo(generic.CreateView):
    template_name = "home/new_modelovehiculo.html"
    model = ModeloVehiculo
    form_class = NewModeloVehiculoForm
    success_url = reverse_lazy("home:modelovehiculoindex")


class ListarModeloVehiculo(generic.View):
    template_name = "home/modelovehiculos.html"
    context = {}

    def get(self, request):
        self.context = {
            "modelovehiculo": ModeloVehiculo.objects.all()
        }
        return render(request, self.template_name, self.context)


class DetailModeloVehiculo(generic.View):
    template_name = "home/detail_modelovehiculo.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "modelovehiculo": get_object_or_404(ModeloVehiculo, num=pk)
        }
        return render(request, self.template_name, self.context)


class DeleteModeloVehiculo(generic.DeleteView):
    template_name = "home/delete_modelovehiculo.html"
    model = ModeloVehiculo
    success_url = reverse_lazy("home:modelovehiculoindex")
    

class UpdateModeloVehiculo(generic.UpdateView):
    template_name = "home/update_modelovehiculo.html"
    model = ModeloVehiculo
    form_class = UpdateModeloVehiculoForm
    success_url = reverse_lazy("home:modelovehiculoindex")