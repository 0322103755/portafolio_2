from django.urls import path
from home import views

app_name = "home"
urlpatterns = [
    #RUTAS PARA VEHICULOS
    path('', views.Base.as_view(), name="base"),
    path('vehiculo/',views.ListarVehiculo.as_view(), name= 'vehiculoindex'),
    path('new/vehiculo/',views.NewVehiculo.as_view(), name='new_vehiculo'),
    path('delete/vehiculo/<str:pk>/',views.DeleteVehiculo.as_view(), name='delete_vehiculo'),
    path('update/vehiculo/<str:pk>/',views.UpdateVehiculo.as_view(), name='update_vehiculo'),
    path('detail/vehiculo/<str:pk>/',views.DetailVehiculo.as_view(), name='detail_vehiculo'),

    #RUTAS PARA MODELOS
    path('modelovehiculo/',views.ListarModeloVehiculo.as_view(), name= 'modelovehiculoindex'),
    path('new/modelovehiculo/',views.NewModeloVehiculo.as_view(), name='new_modelovehiculo'),
    path('update/modelovehiculo/<str:pk>/',views.UpdateModeloVehiculo.as_view(), name='update_modelovehiculo'),
    path('delete/modelovehiculo/<str:pk>/',views.DeleteModeloVehiculo.as_view(), name='delete_modelovehiculo'),
    path('detail/modelovehiculo/<int:pk>/',views.DetailModeloVehiculo.as_view(), name='detail_modelovehiculo'),


    #RUTAS PARA MARCAS
    path('marcavehiculo/',views.ListarMarcaVehiculo.as_view(), name= 'marcavehiculoindex'),
    path('new/marcavehiculo/',views.NewMarcaVehiculo.as_view(), name='new_marcavehiculo'),
    path('delete/marcavehiculo/<int:pk>/',views.DeleteMarcaVehiculo.as_view(), name='delete_marcavehiculo'),
    path('update/marcavehiculo/<int:pk>/',views.UpdateMarcaVehiculo.as_view(), name='update_marcavehiculo'),
    path('detail/marcavehiculo/<int:pk>/',views.DetailMarcaVehiculo.as_view(), name='detail_marcavehiculo'),
    
]

