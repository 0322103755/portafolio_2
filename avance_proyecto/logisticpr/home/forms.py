from django import forms
from .models import  Usuario, Rol, VehiculoUsuario, Vehiculo, ModeloVehiculo, MarcaVehiculo
#PUT HERE THE FORMS

#//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#HERE ARE THE CLASSES FOR THE MODELS, HERE YOU CAN CREATE THE DIFFERNT CLASSES FOR UPDATE OR CRATE DIFFERENT MODELS
#CLASSES:

#USUARIO
class UpdateUsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = [

        ]

class NewUsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = [

        ]

#ROL
class UpdateRolForm(forms.ModelForm):
    class Meta:
        model = Rol
        fields = [

        ]

class NewRolForm(forms.ModelForm):
    class Meta:
        model = Rol
        fields = [

        ]

#VEHICULOUSUARIO
class UpdateVehiculoUsuarioForm(forms.ModelForm):
    class Meta:
        model = VehiculoUsuario
        fields = [

        ]

class NewVehiculoUsuarioForm(forms.ModelForm):
    class Meta:
        model = VehiculoUsuario
        fields = [

        ]

#VEHICULO
class UpdateVehiculoForm(forms.ModelForm):
    class Meta:
        model = Vehiculo
        fields = [
            "numSerie",
            "placas",
            "kilometraje",
            "fechaAdquisicion",
            "usaDiesel",
            "conductor"
        ]
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

class NewVehiculoForm(forms.ModelForm):
    class Meta:
        model = Vehiculo
        fields = [
            "numSerie",
            "placas",
            "kilometraje",
            "fechaAdquisicion",
            "usaDiesel",
            "conductor"
        ]
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

#MODELOVEHICULO
class UpdateModeloVehiculoForm(forms.ModelForm):
    class Meta:
        model = ModeloVehiculo
        fields = [
            "num",
            "nombre",
            "consumoCombustible",
            "cantCarga",
            "capPasajeros",
            "vehiculo"
        ]
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

class NewModeloVehiculoForm(forms.ModelForm):
    class Meta:
        model = ModeloVehiculo
        fields = [
            "num",
            "nombre",
            "consumoCombustible",
            "cantCarga",
            "capPasajeros",
            "vehiculo"
        ]

#MARCAVEHICULO
class UpdateMarcaVehiculoForm(forms.ModelForm):
    class Meta:
        model = MarcaVehiculo
        fields = [
            "num",
            "nombre",
            "vehiculo"
        ]
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field_name, field in self.fields.items():
            field.widget.attrs.update({
                'class': 'w-full p-2 border rounded-lg shadow-sm focus:outline-none focus:ring focus:border-blue-300',
                'style':'margin: .5rem 0;',
            })

class NewMarcaVehiculoForm(forms.ModelForm):
    class Meta:
        model = MarcaVehiculo
        fields = [
            "num",
            "nombre",
            "vehiculo"
        ]

